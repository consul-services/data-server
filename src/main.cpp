#include <iostream>

#include "Server/DataServer.h"

int main(int argc, char **argv) {
    int port = 0;
    if (argc == 1) {
        port = 9876;
        std::cout << "Using default port: " << port << std::endl;
    }
    else if (argc == 3) {
        if (std::string(argv[1]) == "-p") {
            port = std::atoi(argv[2]);
            if (port) {
                std::cout << "Using provided port: " << port << std::endl;
            }
        }
    }

    if (port == 0) {
        std::cout << "Invalid arguments provided. You can specify port value by using '-p <port>' option." << std::endl;
        return 1;
    }

    DataServer server(port);
    server.start();
}
