#include "DataServer.h"
#include "HTTPCodes.h"

using bsoncxx::builder::stream::close_array;
using bsoncxx::builder::stream::close_document;
using bsoncxx::builder::stream::document;
using bsoncxx::builder::stream::finalize;
using bsoncxx::builder::stream::open_array;
using bsoncxx::builder::stream::open_document;

DataServer::DataServer(int port)
        : port(port),
          databaseUri("mongodb://localhost:27017"),
          replyMessagesLimit(10)
{
    consul = std::make_unique<ppconsul::Consul>();
    agent  = std::make_unique<ppconsul::agent::Agent>(*consul.get());

    registerServerHandlers();
}

DataServer::~DataServer()
{
    stop();
}

bool DataServer::start(LaunchMode mode)
{
    if (!server.is_running()) {
        if (!registerInConsul()) {
            std::cout << "Failed to register service in consul agent!" << std::endl;
            return false;
        }
        if (!connectDatabase()) {
            std::cout << "Failed to connect to database on '" << databaseUri << std::endl;
            return false;
        }

        serverThread = std::thread(std::bind(&DataServer::threadFunction, this));
        if (mode == LaunchMode::ASYNC) {
            serverThread.detach();
        }
        else {
            serverThread.join();
        }
        return true;
    }
    return true;
}

void DataServer::stop()
{
    if (server.is_running()) {
        server.stop();
        if (serverThread.joinable()) {
            serverThread.join();
        }
    }
}

void DataServer::threadFunction()
{
    server.listen("localhost", port);
}

void DataServer::healthCheckHandler(const httplib::Request &request, httplib::Response &responce)
{
    responce.status = static_cast<int>(HttpCodes::OK);
    responce.set_content("I am OK!", "text/plain");
}

void DataServer::postMessageHandler(const httplib::Request &request, httplib::Response &responce)
{
    auto builder = bsoncxx::builder::stream::document{};

        //Parse request body as json
    auto json = builder << "" << "" << finalize;
    try {
        json = bsoncxx::from_json(request.body);
    }
    catch (const std::exception &e) {
        auto doc = builder << "error" << e.what() << finalize;
        responce.status = static_cast<int>(HttpCodes::BadRequest);
        responce.body = bsoncxx::to_json(doc);
        return;
    }

        //Extract username and message
    auto username = getPropertyAsString(json, "username");
    auto message  = getPropertyAsString(json, "message");
    if (username.empty() || message.empty()) {
        auto doc = builder << "error" << "Invalid request parameters" << finalize;
        responce.status = static_cast<int>(HttpCodes::BadRequest);
        responce.body = bsoncxx::to_json(doc);
        return;
    }

        //Write to database
    auto documentToInsert = builder << "username" << username << "message" << message << finalize;
    bsoncxx::stdx::optional<mongocxx::result::insert_one> result;
    try {
        result = collection.insert_one(std::move(documentToInsert));
    }
    catch (const std::exception &e) {
        auto doc = builder << "error" << e.what() << finalize;
        responce.status = static_cast<int>(HttpCodes::BadRequest);
        responce.body = bsoncxx::to_json(doc);
        return;
    }

    //Build reply
    if (result) {
        mongocxx::options::find options;
        options.limit(replyMessagesLimit);
        auto documents = collection.find(builder << "username" <<
                                                 open_document <<
                                                 "$ne" << username <<
                                                 close_document <<
                                                 finalize, options);
        auto jsonArray = bsoncxx::builder::basic::array{};
        for(auto d : documents) {
            auto doc = builder << "username" << d["username"].get_utf8().value <<
                                  "message" << d["message"].get_utf8().value <<
                                  finalize;
            jsonArray.append(doc);
        }

        auto responceDocument = builder
                << "messages" << jsonArray
                << finalize;
        responce.body = bsoncxx::to_json(responceDocument);
        responce.status = static_cast<int>(HttpCodes::OK);
        return;
    }
    else {
        responce.body = "MongoDB unavailable";
        responce.status = static_cast<int>(HttpCodes::ServiceUnavailable);
        return;
    }
}

bool DataServer::registerInConsul()
{
    try {
        auto check = ppconsul::agent::HttpCheck{"http://localhost:" + std::to_string(port) + "/health", std::chrono::seconds(2)};
        agent->registerService(
                ppconsul::agent::kw::name = "DataServer",
                ppconsul::agent::kw::id = "DataServer:" + std::to_string(port),
                ppconsul::agent::kw::port = port,
                ppconsul::agent::kw::tags = {"http"},
                ppconsul::agent::kw::check = check);
        return true;
    }
    catch (...) {
        return false;
    }
}

bool DataServer::connectDatabase() {
    client = mongocxx::client{mongocxx::uri{}};
    database = client["storage"];
    if (database) {
        collection = database["messages"];
        if (collection) {
            return true;
        }
    }
    return false;
}

void DataServer::setDatabaseHost(const std::string &host, int p) {
    databaseUri = host + ':' + std::to_string(p);
}

void DataServer::registerServerHandlers() {
    server.Get("/health", std::bind(&DataServer::healthCheckHandler, this, std::placeholders::_1, std::placeholders::_2));
    server.Post("/message", std::function<void(const httplib::Request &, httplib::Response &)>(
            std::bind(&DataServer::postMessageHandler, this, std::placeholders::_1, std::placeholders::_2)));
}

void DataServer::limitReplyMessagesCount(unsigned int limit) {
    replyMessagesLimit.store(limit);
}

std::string DataServer::getPropertyAsString(const bsoncxx::document::value &doc, const std::string &propertyName) const {
    auto propertyElement = doc.view()[propertyName];
    if (propertyElement && propertyElement.type() == bsoncxx::type::k_utf8) {
        return propertyElement.get_utf8().value.to_string();
    }
    return std::string();
}

