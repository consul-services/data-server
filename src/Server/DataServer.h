#ifndef SERVER_DATA_SERVER_H
#define SERVER_DATA_SERVER_H

#include <cstdint>
#include <thread>
#include <memory>
#include <atomic>

#include <bsoncxx/v_noabi/bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/v_noabi/bsoncxx/builder/stream/helpers.hpp>
#include <bsoncxx/v_noabi/bsoncxx/json.hpp>

#include <mongocxx/v_noabi/mongocxx/instance.hpp>
#include <mongocxx/v_noabi/mongocxx/client.hpp>
#include <mongocxx/v_noabi/mongocxx/uri.hpp>

#include <cpp-httplib/httplib.h>
#include <ppconsul/agent.h>

class DataServer {
public:
    enum class LaunchMode {
        SYNC,
        ASYNC
    };
    explicit DataServer(int port = 9876);
    ~DataServer();

    bool start(LaunchMode mode = LaunchMode::SYNC);
    void stop();

    void setDatabaseHost(const std::string &host, int port);

    void limitReplyMessagesCount(unsigned int limit);

private:
    std::thread serverThread;

    httplib::Server server;
    const int port;
    std::atomic<unsigned int> replyMessagesLimit;

    std::unique_ptr<ppconsul::Consul> consul;
    std::unique_ptr<ppconsul::agent::Agent> agent;

    mongocxx::instance instance;
    mongocxx::client client;
    mongocxx::database database;
    mongocxx::collection collection;
    std::string databaseUri;

private:
    void threadFunction();

    bool registerInConsul();
    bool connectDatabase();

    void registerServerHandlers();
    void healthCheckHandler(const httplib::Request &request, httplib::Response &responce);
    void postMessageHandler(const httplib::Request &request, httplib::Response &responce);

    std::string getPropertyAsString(const bsoncxx::document::value &doc, const std::string &propertyName) const;
};

#endif //SERVER_DATA_SERVER_H
